from django.contrib import admin
from .models import Recipe,RecipeStep,Ingredient

# Register your models here.
admin.site.register(Recipe)

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "step_number",
        "instruction",
        "id",
    )

@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = (
        "amount",
        "food_item",
    )
